package com.lunamezzogiorno.io;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;
import java.io.*;
import java.util.List;

/**
 * Author of code: Katlin Kalde (30.08.23)
 * Task: Write a program that will find all .docx files
 * in a given directory and subdirectories and write their context into a textfile
 *
 * Helps to solve a problem:
 * The contents (text) from bunch of files in a directory and its subdirectories
 * are needed to copy into one separate file.
 *
 * Got the idea from:
 * https://www.geeksforgeeks.org/java-program-to-extract-paragraphs-from-a-word-document/
 * With Maven, uses libraries (see pom.xml):
 * * org.apache.poi
 * * org.apache.logging.log4j (log4j-api and log4j-core)
 * (JDK 20.0.1)
 */

public class App {
    public static void main(String[] args) {
        //The path of the directory and
        //its subdirectories containing .docx-s:
        String pathIn = "./TEST/final";
        File path = new File(pathIn);
        //Output file:
        String pathOut = "./TEST/test.txt";
        var files = getWordFiles(path);

        assert files != null;
        readFromWord(pathOut, files);
    }

    private static List <Path> getWordFiles(File path) {
        try(Stream<Path> walkThroughDirectories = Files.walk(path.toPath())) {
            return walkThroughDirectories
                    .filter(f-> f.toFile().isFile())        //filter out files
                    .filter(f -> (f.toString().endsWith(".docx")))
                    .toList();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static void readFromWord(String pathOut, List <Path> paths) {
        try(var fr = new FileWriter(pathOut, true);
            var br = new BufferedWriter(fr)) {
            for (Path p : paths) {
                FileInputStream fin = new FileInputStream(p.toString());
                // Create a document object for the .docx
                XWPFDocument document = new XWPFDocument(fin);

                // retrieve the list of paragraphs from .docx
                List<XWPFParagraph> paragraphs
                        = document.getParagraphs();

                for (XWPFParagraph par : paragraphs) {
                    br.write(par.getText() + "\n");
                }
                document.close();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
